
$(document).ready(function() {
    const services = document.querySelectorAll('.service_item');
    const tags = document.querySelector('.posts_headMenu').querySelectorAll('.posts_a');
    const postsFirst = document.querySelector('.posts_blockFirst').querySelectorAll('.posts_itemF');
    const postsSecond = document.querySelector('.posts_blockSecond').querySelectorAll('.posts_itemS');
    const cursorLeft = document.querySelector('.partners_left');
    const cursorRight = document.querySelector('.partners_right');
    function resize () {
        const width = window.innerWidth;
        if (width < 749) {
            services.forEach((elem, i)=> {
                elem.style.display = (i > 1) ? 'none' : 'flex';
            });
            tags.forEach((elem, i)=> {
                elem.style.display = (i < 5) ? 'none' : 'inline';
            });
            postsFirst.forEach((elem)=> {
                elem.querySelector('.posts_pF').style.display = 'none';
            });
            postsSecond.forEach((elem, i)=> {
                elem.style.display = (i > 1) ? 'none' : 'flex';
            });
            cursorLeft.style.display = 'none';
            cursorRight.style.display = 'none';
        }
        else if (width >= 750 && width < 1000){
            services.forEach((elem)=> {
                elem.style.display = 'flex';
            });
            tags.forEach((elem, i)=> {
                elem.style.display = (i > 1 && i < 5) ? 'none' : 'inline';
            });
            postsFirst.forEach((elem)=> {
                elem.querySelector('.posts_pF').style.display = 'inline';
            });
            postsSecond.forEach((elem)=> {
                elem.style.display = 'flex';
            });
            cursorLeft.style.display = 'none';
            cursorRight.style.display = 'none';
        }
        else {
            tags.forEach((elem)=> {
                elem.style.display = 'inline';
            });
            postsSecond.forEach((elem, i)=> {
                elem.style.display = (i > 2) ? 'none' : 'flex';
            });
            cursorLeft.style.display = 'flex';
            cursorRight.style.display = 'flex';
        }
    }
    function dropDown (e) {
        e.preventDefault();
        document.querySelector('.header').style.display = 'none';
        document.querySelector('.dropDown').style.display = 'flex';
    }
    function hide (e) {
        e.preventDefault();
        document.querySelector('.header').style.display = 'flex';
        document.querySelector('.dropDown').style.display = 'none';
    }
    resize();
    window.addEventListener("resize", resize);
    document.querySelector('.header_imgMenu').addEventListener("click", dropDown, false);
    document.querySelector('.dropDown_img').addEventListener("click", hide, false);
});



